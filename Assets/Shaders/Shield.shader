﻿
Shader "MyShaders/Shield"
{
    Properties
    {
        [Header(Lightnings)]
        MyNoise ("Noise", 2D) = "white" {}
        m_MyColor("Multiply", Color) = (1.0, 1.0, 1.0, 1.0)
        _Flow2 ("Velocity", Vector) = (-1.0, -1.0, 0.0, 0.0)
        [Space]
        _NoiseMaxValue ("Noise Max Value", Range(0.0, 1.0)) = 0.5
        _NoiseMinValue ("Noise Min Value", Range(0.0, 1.0)) = 0.4

		[Header(Background)]
        [Toggle(Should_Use_Background_Image)]
        _ShouldUseImage ("Should Use Background Image", Range(0.0, 1.0)) = 1.0
        _BgColor("Background Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _BgImage ("Background", 2D) = "white" {}

    }
    SubShader
    {        
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
        LOD 100
        Cull Off
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D MyNoise;
            half4 m_MyColor;
            half _NoiseMaxValue;
            half _NoiseMinValue;

            fixed _ShouldUseImage;
            sampler2D _BgImage;
            half4 _BgColor;
            half2 _Flow2;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD2;
                float2 uv_background : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD2;
                float2 uv_background : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv1 = v.uv1;
                o.uv2 = v.uv2;
                o.uv1 += _Flow2 * _Time.y;
                o.uv2 -= _Flow2 * 1.5 * _Time.y;
                o.uv_background = v.uv_background;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                half4 temp1 = tex2D(MyNoise, i.uv1 /3);
                half4 temp2 = tex2D(MyNoise, i.uv2/4);

                half4 electric = (temp1 + temp2)/2;

                half4 background = lerp(_BgColor,tex2D(_BgImage, i.uv_background), _ShouldUseImage);

                if (electric.x < _NoiseMaxValue && electric.x > _NoiseMinValue) {electric = m_MyColor;}
                else { electric = background;}
                
                return electric;
            }
            ENDCG
        }
    }
}