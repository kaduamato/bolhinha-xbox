﻿using UnityEngine;

public class ObstacleSpawner : Spawner
{
    public override void SpawnObject(string obstacleTag, int direction)
    {
        GameObject obstacle = ObjectPooler.MyPooler.GetPooledObj(obstacleTag);
        Obstacle obstacleComponent = obstacle.GetComponent<Obstacle>();
        if (obstacle != null)
        {
            switch (direction)
            {
                case 0:
                    SetObstacle(obstacle, obstacleComponent, 0);
                    break;
                case 1:
                    SetObstacle(obstacle, obstacleComponent, 1);
                    break;
                case 2:
                    SetObstacle(obstacle, obstacleComponent, 2);
                    break;
                case 3:
                    SetObstacle(obstacle, obstacleComponent, 3);
                    break;
                case 4:
                    SetObstacle(obstacle, obstacleComponent, 4);
                    break;
                case 5:
                    SetObstacle(obstacle, obstacleComponent, 5);
                    break;
            }
        }
    }

    void SetObstacle(GameObject obstacle, Obstacle obstacleComponent, int direction)
    {
        switch (direction)
        {
            case 0:
                obstacle.transform.position = new Vector3(m_MSO.obstaclesInfo.rightToLeftPos.x, Random.Range(m_MSO.obstaclesInfo.yLimitBottom, m_MSO.obstaclesInfo.yLimitTop), m_MSO.obstaclesInfo.rightToLeftPos.z);
                obstacle.transform.rotation = Quaternion.Euler(m_MSO.obstaclesInfo.rightOrientation[0], m_MSO.obstaclesInfo.rightOrientation[1], m_MSO.obstaclesInfo.rightOrientation[2]);
                obstacleComponent.Direction = 0;
                break;
            case 1:
                obstacle.transform.position = new Vector3(m_MSO.obstaclesInfo.leftToRightPos.x, Random.Range(m_MSO.obstaclesInfo.yLimitBottom, m_MSO.obstaclesInfo.yLimitTop), m_MSO.obstaclesInfo.leftToRightPos.z);
                obstacle.transform.rotation = Quaternion.Euler(m_MSO.obstaclesInfo.leftOrientation[0], m_MSO.obstaclesInfo.leftOrientation[1], m_MSO.obstaclesInfo.leftOrientation[2]);
                obstacleComponent.Direction = 1;
                break;
            case 2:
                obstacle.transform.position = m_MSO.obstaclesInfo.dLeftUpToRightDownPos;
                obstacle.transform.rotation = Quaternion.Euler(m_MSO.obstaclesInfo.leftUpToRightDownOrientation[0], m_MSO.obstaclesInfo.leftUpToRightDownOrientation[1], m_MSO.obstaclesInfo.leftUpToRightDownOrientation[2]);
                obstacleComponent.Direction = 2;
                break;
            case 3:
                obstacle.transform.position = m_MSO.obstaclesInfo.dLeftDownToRightUpPos;
                obstacle.transform.rotation = Quaternion.Euler(m_MSO.obstaclesInfo.leftDownToRightUpOrientation[0], m_MSO.obstaclesInfo.leftDownToRightUpOrientation[1], m_MSO.obstaclesInfo.leftDownToRightUpOrientation[2]);
                obstacleComponent.Direction = 3;
                break;
            case 4:
                obstacle.transform.position = m_MSO.obstaclesInfo.dRightUpToLeftDownPos;
                obstacle.transform.rotation = Quaternion.Euler(m_MSO.obstaclesInfo.rightUpToLeftDownOrientation[0], m_MSO.obstaclesInfo.rightUpToLeftDownOrientation[1], m_MSO.obstaclesInfo.rightUpToLeftDownOrientation[2]);
                obstacleComponent.Direction = 4;
                break;
            case 5:
                obstacle.transform.position = m_MSO.obstaclesInfo.dRightDownToLeftUpPos;
                obstacle.transform.rotation = Quaternion.Euler(m_MSO.obstaclesInfo.rightDownToLeftUpOrientation[0], m_MSO.obstaclesInfo.rightDownToLeftUpOrientation[1], m_MSO.obstaclesInfo.rightDownToLeftUpOrientation[2]);
                obstacleComponent.Direction = 5;
                break;
        }

        obstacleComponent.SkinObject.material.mainTexture = m_MSO.obstaclesInfo.catTextures[Random.Range(0, m_MSO.obstaclesInfo.catTextures.Length)];
        obstacle.SetActive(true);
    }
}