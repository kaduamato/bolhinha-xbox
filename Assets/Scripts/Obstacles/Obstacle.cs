﻿using UnityEngine;

public class Obstacle : SpawnableObject
{
    [SerializeField] SkinnedMeshRenderer m_SkinObject;
    [SerializeField] Animator m_Animator;
    public SkinnedMeshRenderer SkinObject { get => m_SkinObject; set => m_SkinObject = value; }

    const float m_DiagonalMovementSpeedMultiplier = 0.7071f;

    const int m_ObstacleLayer = 9;

    protected override void Update()
    {
        Physics.IgnoreLayerCollision(m_ObstacleLayer, m_ObstacleLayer, true);

        m_Speed = m_MSO.obstaclesInfo.obstaclesSpeed * Time.deltaTime;

        switch (m_Direction)
        {
            case 0:
                transform.position += Vector3.left * m_Speed;
                break;
            case 1:
                transform.position += Vector3.right * m_Speed;
                break;
            case 2:
                transform.position += m_MSO.obstaclesInfo.leftUpToRightDownShortcut * m_DiagonalMovementSpeedMultiplier * m_Speed;
                break;
            case 3:
                transform.position += m_MSO.obstaclesInfo.leftDownToRightUpShortcut * m_DiagonalMovementSpeedMultiplier * m_Speed;
                break;
            case 4:
                transform.position += m_MSO.obstaclesInfo.rightUpToLeftDownShortcut * m_DiagonalMovementSpeedMultiplier * m_Speed;
                break;
            case 5:
                transform.position += m_MSO.obstaclesInfo.rightDownToLeftUpShortcut * m_DiagonalMovementSpeedMultiplier * m_Speed;
                break;
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);
    }
}