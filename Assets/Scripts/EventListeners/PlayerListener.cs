﻿public class PlayerListener : GameEventListener
{
    Collisions m_Parent;

    void Awake()
    {
        m_Parent = GetComponent<Collisions>();    
    }

    public override void OnEventRaised(string eventName)
    {
        m_Parent.OnPowerUpCollision();
    }
}