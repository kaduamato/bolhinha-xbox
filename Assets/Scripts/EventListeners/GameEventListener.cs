﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameEventListener : MonoBehaviour
{
    public GameEventSO[] m_Event;

    private void OnEnable()
    {
        for (int i = 0; i < m_Event.Length; i++)
        {
            m_Event[i].RegisterListener(this);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < m_Event.Length; i++)
        {
            m_Event[i].UnregisterListener(this);
        }
    }

    public virtual void OnEventRaised(string eventName)
    {
       
    }
}
