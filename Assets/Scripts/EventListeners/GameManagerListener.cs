﻿public class GameManagerListener : GameEventListener
{
    GameManager m_Parent;

    private void Awake()
    {
        m_Parent = GetComponent<GameManager>();
    }

    public override void OnEventRaised(string funcName)
    {
        m_Parent.Invoke(funcName, 0); 
    }
}