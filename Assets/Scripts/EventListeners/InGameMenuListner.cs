﻿public class InGameMenuListner : GameEventListener
{
    InGameMenuController m_Parent;
    private void Awake()
    {
        m_Parent = GetComponent<InGameMenuController>();
    }


    public override void OnEventRaised(string eventName)
    {
        m_Parent.Invoke(eventName, 0);
    }
}
