﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class SkinMenu : MonoBehaviour
{
    [SerializeField] ManagerSO m_MSO;
    [SerializeField] GameObject[] m_Skins;
    [SerializeField] GameObject[] m_Satus;
    [SerializeField] Text m_SkinText;
    int m_CurrentSlectedSkin = 0;

    Rewired.Player m_Player;

    bool m_CanGo;

    private void OnEnable()
    {
        m_CurrentSlectedSkin = m_MSO.generalInfo.playerSkin;
        SetSkinSelector();
    }

    private void OnDisable()
    {
        m_CanGo = true;
    }

    void Awake()
    {
        m_Player = ReInput.players.GetPlayer("Player0");
        m_CanGo = true;
    }

    private void Update()
    {
        if (m_CanGo)
        {
            if (m_Player.GetAxis("Left Stick Horizontal") > 0.5f)
            {
                AdvenceSkin();
                SetSkinSelector();
            }
            if (m_Player.GetAxis("Left Stick Horizontal") < -0.5f)
            {
                BackSkin();
                SetSkinSelector();
            }
            m_CanGo = false;
            StartCoroutine(Delay());
        }
    }

    void SetSkinSelector()
    {
        CheckStatus();
        m_Skins[0].GetComponent<Image>().sprite = SetSkin(m_CurrentSlectedSkin);
        m_Satus[0].GetComponent<Image>().sprite = SetStatus(m_CurrentSlectedSkin);
        m_SkinText.text = SetSkinName(m_CurrentSlectedSkin);

        if(m_CurrentSlectedSkin == 0)
        {
            m_Skins[1].GetComponent<Image>().sprite = SetSkin(m_MSO.skinKeeper.MySkins.Length - 1);
            m_Satus[1].GetComponent<Image>().sprite = SetStatus(m_MSO.skinKeeper.MySkins.Length - 1);
        }
        else
        {
            m_Skins[1].GetComponent<Image>().sprite = SetSkin(m_CurrentSlectedSkin - 1);
            m_Satus[1].GetComponent<Image>().sprite = SetStatus(m_CurrentSlectedSkin - 1);
        }


        if(m_CurrentSlectedSkin == m_MSO.skinKeeper.MySkins.Length - 1)
        {
            m_Skins[2].GetComponent<Image>().sprite = SetSkin(0);
            m_Satus[2].GetComponent<Image>().sprite = SetStatus(0);
        }
        else
        {
            m_Skins[2].GetComponent<Image>().sprite = SetSkin(m_CurrentSlectedSkin + 1);
            m_Satus[2].GetComponent<Image>().sprite = SetStatus(m_CurrentSlectedSkin + 1);
        }

        CheckStatus();

    }

    void CheckStatus()
    {
        for(int i = 0; i < m_Satus.Length; i++)
        {
            if(m_Satus[i].GetComponent<Image>().sprite == null)
            {
                m_Satus[i].SetActive(!m_Satus[i].activeSelf);
            }
        }
    }

    void AdvenceSkin()
    {
        if (m_CurrentSlectedSkin >= m_MSO.skinKeeper.MySkins.Length - 1)
        {
            m_CurrentSlectedSkin = 0;
        }
        else
        {
            m_CurrentSlectedSkin++;
        }
    }

    void BackSkin()
    {
        if (m_CurrentSlectedSkin <= 0)
        {
            m_CurrentSlectedSkin = m_MSO.skinKeeper.MySkins.Length - 1;
        }
        else
        {
            m_CurrentSlectedSkin--;
        }
    }

    Sprite SetSkin(int skin)
    {
        if (skin <= m_MSO.generalInfo.unlockedSkins - 1)
        {
            return m_MSO.skinKeeper.MySkins[skin].SkinSprite;
        }
        else
        {
            return m_MSO.generalInfo.blockedSkin;
        }
    }

    string SetSkinName(int skin)
    {
        if (skin <= m_MSO.generalInfo.unlockedSkins - 1)
        {
            return m_MSO.skinKeeper.MySkins[skin].SkinName;
        }
        else
        {
            return m_MSO.generalInfo.blockedSkinName;
        }
    }

    Sprite SetStatus(int skin)
    {
        if(skin == m_MSO.generalInfo.playerSkin)
        {
            return m_MSO.generalInfo.statusSprites[0];
        }
        else if (skin <= m_MSO.generalInfo.unlockedSkins - 1)
        {
            return  null;
        }
        else
        {
            return m_MSO.generalInfo.statusSprites[1]; 
        }
    }
    

    public void ChangeSkin()
    {
        if (m_CurrentSlectedSkin <= m_MSO.generalInfo.unlockedSkins - 1)
        {
            m_MSO.generalInfo.playerSkin = m_CurrentSlectedSkin;
            PlayerPrefs.SetInt("PlayerSkin", m_CurrentSlectedSkin);
            SetSkinSelector();
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(m_MSO.generalInfo.controllerDelay);
        m_CanGo = true;
    }
}
