﻿using UnityEngine;

public class SpawnableObject : MonoBehaviour
{
    [SerializeField] protected ManagerSO m_MSO;

    protected int m_Direction;
    public int Direction { set => m_Direction = value; }

    protected float m_Speed;
    
    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("GameArea"))
        {
            gameObject.SetActive(false);
        }
    }
}