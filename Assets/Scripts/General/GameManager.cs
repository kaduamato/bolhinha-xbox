﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(QuitGame))]
[RequireComponent(typeof(GameManagerListener))]
public class GameManager : MonoBehaviour
{
    [SerializeField] ManagerSO m_MSO;

    private void Awake()
    {
        DontDestroyOnLoad(this);

        Cursor.visible = false;
    }

    private void Start()
    {
        ResetGeralInformations();
        SetPlayerPrefs();
    }
   
    private void Update()
    {
        if (m_MSO.generalInfo.soundConfig)
        {
            UnMute();
        }
        else
        {
            Mute();
        }
    }

    void ResetGeralInformations()
    {
        m_MSO.generalInfo.isGameOver = false;
        m_MSO.generalInfo.soundConfig = true;
        m_MSO.player.playerState = 0;
        m_MSO.generalInfo.score = 0;
        m_MSO.generalInfo.highScore = 0;
        m_MSO.generalInfo.unlockedSkins = 1;
        m_MSO.generalInfo.playerSkin = 0;
    }

    void ResetGame()
    {
        m_MSO.generalInfo.isGameOver = false;
        m_MSO.generalInfo.isPaused = false;
        m_MSO.generalInfo.score = 0;
        m_MSO.player.playerState = 0;
        m_MSO.generalInfo.master.SetFloat("GameplayTracks", 0.0f);
    }

    void SetPlayerPrefs()
    {
        m_MSO.generalInfo.playerSkin = PlayerPrefs.GetInt("PlayerSkin");
        m_MSO.generalInfo.highScore = PlayerPrefs.GetInt("HighScore");
        m_MSO.generalInfo.unlockedSkins = PlayerPrefs.GetInt("UnlockedSkins");
        m_MSO.generalInfo.soundConfig = PlayerPrefs.GetInt("SoundConfig") == 0 ? false : true;
    }

    public void ResetPlayerPrefs()
    {
        PlayerPrefs.SetInt("PlayerSkin", 0);
        PlayerPrefs.SetInt("HighScore", 0);
        PlayerPrefs.SetInt("UnlockedSkins", 1);
        PlayerPrefs.SetInt("SoundConfig", 1);
    }

    void OnBackMenu()
    {
        OnGameResume();
        SceneManager.LoadScene("Loading");
        StartCoroutine(LoadingTime("Menu"));
    }

    void OnRestartGame()
    {
        OnGameResume();
        ResetGame();
        SceneManager.LoadScene("Loading");
        StartCoroutine(LoadingTime("Game"));
    }

    void OnGamePaused()
    {
       Time.timeScale = 0.0f;
    }

    void OnGameResume()
    {
        Time.timeScale = 1.0f;
    }

    void OnPlayGame()
    {
        SceneManager.LoadScene("Loading");
        StartCoroutine(LoadingTime("Game"));
        OnRestartGame();
    }

    void OnQuitGame() 
    {
        GetComponent<QuitGame>().Quit();
    }

    void OnSplashFinish()
    {
        SceneManager.LoadScene("Menu");
    }

    void Mute()
    {
        m_MSO.generalInfo.master.SetFloat("Master", -80.0f);
    }

    void UnMute()
    {
        m_MSO.generalInfo.master.SetFloat("Master", 0.0f);
    }

    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            AudioListener.pause = true;
        }
        else
        {
            AudioListener.pause = false;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            AudioListener.pause = true;
        }
        else
        {
            AudioListener.pause = false;
        }
    }

    IEnumerator LoadAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        int cout = 0;
        while (!asyncLoad.isDone && cout == 200)
        {
            yield return null;
        }
    }

    IEnumerator LoadingTime(string sceneName)
    {
        yield return new WaitForSeconds(m_MSO.generalInfo.loadTime);
        StartCoroutine(LoadAsyncScene(sceneName));
    }
}
