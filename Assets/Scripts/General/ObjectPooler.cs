using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjPoolIten
{
    public GameObject objToPool;
    public string poolTag;
    public int amountToPool;
    public bool shouldExpand;
}

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler MyPooler;

    public List<ObjPoolIten> itensToPool;
    public Dictionary<string, List<GameObject>> allPooledObjs;

    private void Awake()
    {
        MyPooler = this;
        allPooledObjs = new Dictionary<string, List<GameObject>>();
        foreach(ObjPoolIten item in itensToPool)
        {
            allPooledObjs.Add(item.poolTag, new List<GameObject>());
            for(int i = 0; i < item.amountToPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.objToPool);
                obj.SetActive(false);
                allPooledObjs[item.poolTag].Add(obj);
            }
        }
    }

    public GameObject GetPooledObj(string tag)
    {
        List<GameObject> pooledObjs = allPooledObjs[tag];

        for (int i = 0; i < pooledObjs.Count; i++)
        {
            if (pooledObjs[i] != null && !pooledObjs[i].activeInHierarchy)
            {
                return pooledObjs[i];
            }
        }
        foreach(ObjPoolIten item in itensToPool)
        {
            if(item.poolTag == tag)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objToPool);
                    obj.SetActive(false);
                    pooledObjs.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }

    public void DisableAll()
    {
        foreach (var pooledObjs in allPooledObjs)
        {
            foreach(GameObject i in pooledObjs.Value)
            {
                i.SetActive(false);
            }
        }
    }
}