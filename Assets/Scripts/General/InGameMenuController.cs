﻿using UnityEngine;
using UnityEngine.UI;
using Rewired;
using Rewired.Integration.UnityUI;

public class InGameMenuController : MonoBehaviour
{
    [SerializeField] ManagerSO m_MSO;
    [SerializeField] GameObject m_PauseMenu;
    [SerializeField] Button m_SoundButton;
    [SerializeField] GameObject m_GameOverMenu;
    [SerializeField] AudioSource[] m_Audios;
    [SerializeField] RewiredEventSystem m_EventSystem;
    [SerializeField] GameObject[] m_FirstButton;

    Rewired.Player m_Player;

    private void Awake()
    {
        m_Player = ReInput.players.GetPlayer("Player0");

        ChangeControlMap(false, true, false);

        for (int i = 0; i < m_Audios.Length; ++i)
        {
            m_Audios[i].ignoreListenerPause = true;
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            if (!m_MSO.generalInfo.isGameOver)
            {
                PauseGame();
            }
            ChangeControlMap(false, false, false);
        }
        else
        {
            ChangeControlMap(true, false, false);
        }
    }

    private void OnEnable()
    {
        m_SoundButton.image.sprite = m_MSO.generalInfo.soundButtonSprites[m_MSO.generalInfo.soundConfig.GetHashCode()];
    }

    void Update()
    {
        if (m_PauseMenu.activeSelf)
        {
            m_SoundButton.image.sprite = m_MSO.generalInfo.soundButtonSprites[m_MSO.generalInfo.soundConfig.GetHashCode()];
            
            if(m_Player.GetButtonDown("Return Pause"))
            {
                ResumeGame();
                ChangeControlMap(false, true, false);
            }
        }
        else if (m_Player.GetButtonDown("Pause Game"))
        {
            PauseGame();
            ChangeControlMap(true, false, false);
        }
    }

    public void ChangeControlMap(bool pauseValue, bool gameplayValue, bool menuValue)
    {
        m_Player.controllers.maps.SetMapsEnabled(gameplayValue, "Game");
        m_Player.controllers.maps.SetMapsEnabled(pauseValue, "Pause");
        m_Player.controllers.maps.SetMapsEnabled(menuValue, "Menu");
    }

    public void ResumeGame() 
    {
        m_MSO.gameResume.Raise();
        m_EventSystem.SetSelectedGameObject(m_SoundButton.gameObject);
        m_SoundButton.Select();
        m_PauseMenu.SetActive(false);
        m_MSO.generalInfo.isPaused = false;
        ChangeControlMap(false, true, false);
    }

    public void RestartGame()
    {
        m_MSO.restartGame.Raise();
        m_MSO.generalInfo.isPaused = false;
    }

    public void BackToMenu()
    {
        m_MSO.backMenu.Raise();
    }

    public void ChangeAudioState()
    {
        m_MSO.generalInfo.soundConfig = !m_MSO.generalInfo.soundConfig;
        PlayerPrefs.SetInt("SoundConfig", m_MSO.generalInfo.soundConfig.GetHashCode());
    }

    public void PauseGame()
    {
        m_MSO.gamePaused.Raise();
        m_PauseMenu.SetActive(true);
        m_MSO.generalInfo.isPaused = true;
        m_EventSystem.SetSelectedGameObject(m_FirstButton[0]);
        m_EventSystem.currentSelectedGameObject.GetComponent<Button>().Select();
    }

    public void OnDeath()
    {
        m_GameOverMenu.SetActive(true);
        ChangeControlMap(true, false, false);
        m_FirstButton[1].GetComponent<Button>().Select();
        m_EventSystem.SetSelectedGameObject(m_FirstButton[1]);
    }

    public void PlayConfirmAudio()
    {
        if (!m_Audios[0].isPlaying)
            m_Audios[0].Play();
    }

    public void PlayCancelAudio()
    {
        if (!m_Audios[1].isPlaying)
            m_Audios[1].Play();
    }
}