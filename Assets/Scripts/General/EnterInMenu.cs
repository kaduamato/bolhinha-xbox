﻿using System.Collections;
using UnityEngine;

public class EnterInMenu : MonoBehaviour
{
    const float m_AnimationTime = 0.2f;
    const float m_DelayStartAnimation = 0.3f;

    int count = 0;

    [SerializeField] GameObject[] m_Tabs;
    [SerializeField] AudioSource m_WindowCloseAudio;
    [SerializeField] AudioSource m_WindowOpenAudio;

    private void OnEnable()
    {
        StartCoroutine(StartAnimation());
    }
    
    public void CloseMenu()
    {
        StartCoroutine(RunAnimation(false));
        if (!m_WindowCloseAudio.isPlaying)
            m_WindowCloseAudio.Play();
    }
    
    IEnumerator StartAnimation()
    {
        yield return new WaitForSeconds(m_DelayStartAnimation);
        StartCoroutine(RunAnimation(true));
    }

    IEnumerator RunAnimation(bool activation)
    {
        m_Tabs[count].SetActive(activation);
        if (!m_WindowOpenAudio)
            m_WindowOpenAudio.Play();

        yield return new WaitForSeconds(m_AnimationTime);

        if (activation)
        {
            if(count < m_Tabs.Length - 1)
            {
                count++;
                StartCoroutine(RunAnimation(activation));
            }
        }
        else
        {
            if (count > 0)
            {
                count--;
                StartCoroutine(RunAnimation(activation));
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
    }
}