﻿using System.Collections;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] ManagerSO m_MSO;
    [SerializeField] ObstacleSpawner m_ObsSpawner;
    [SerializeField] PowerUpSpawner m_PowSpawner;

    void Start()
    {
        StartCoroutine(SpawnObstacle(m_MSO.spawnManagerInfo.currentLevel));
        StartCoroutine(SpawnPowerUp());
    }

    void Update()
    {
        if (m_MSO.generalInfo.isGameOver)
        {
            StopAllCoroutines();
        }

        if (m_MSO.generalInfo.score == m_MSO.spawnManagerInfo.scoreLevels[0])
        {
            m_MSO.spawnManagerInfo.currentLevel = 0;
        }
        if (m_MSO.generalInfo.score == m_MSO.spawnManagerInfo.scoreLevels[1])
        {
            m_MSO.spawnManagerInfo.currentLevel = 1;
        }
        if (m_MSO.generalInfo.score == m_MSO.spawnManagerInfo.scoreLevels[2])
        {
            m_MSO.spawnManagerInfo.currentLevel = 2;
        }
        if (m_MSO.generalInfo.score == m_MSO.spawnManagerInfo.scoreLevels[3])
        {
            m_MSO.spawnManagerInfo.currentLevel = 3;
        }
    }

    IEnumerator SpawnObstacle(int levelIndex)
    {
        yield return new WaitForSeconds(m_MSO.spawnManagerInfo.levelsObsSpawnRates[levelIndex]);

        switch (m_MSO.spawnManagerInfo.currentLevel)
        {
            case 0:
                m_ObsSpawner.SpawnObject(m_MSO.spawnManagerInfo.obstacles[Random.Range(0, m_MSO.spawnManagerInfo.obstacles.Length)], Random.Range(0, 2));
                break;
            case 1:
                m_ObsSpawner.SpawnObject(m_MSO.spawnManagerInfo.obstacles[Random.Range(0, m_MSO.spawnManagerInfo.obstacles.Length)], Random.Range(0, 4));
                break;
            case 2:
                m_ObsSpawner.SpawnObject(m_MSO.spawnManagerInfo.obstacles[Random.Range(0, m_MSO.spawnManagerInfo.obstacles.Length)], Random.Range(0, 5));
                break;
            case 3:
                m_ObsSpawner.SpawnObject(m_MSO.spawnManagerInfo.obstacles[Random.Range(0, m_MSO.spawnManagerInfo.obstacles.Length)], Random.Range(0, 6));
                break;
        }

        if (!m_MSO.generalInfo.isGameOver)
        {
            StartCoroutine(SpawnObstacle(m_MSO.spawnManagerInfo.currentLevel));
        }
    }

    IEnumerator SpawnPowerUp()
    {
        yield return new WaitForSeconds(m_MSO.spawnManagerInfo.powerUpSpawnRate);
        m_PowSpawner.SpawnObject(m_MSO.spawnManagerInfo.powerUps[Random.Range(0, m_MSO.spawnManagerInfo.powerUps.Length)], Random.Range(0, 2));

        if (!m_MSO.generalInfo.isGameOver)
        {
            StartCoroutine(SpawnPowerUp());
        }
    }
}