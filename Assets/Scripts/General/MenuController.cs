﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Rewired;

public class MenuController : MonoBehaviour
{
    const float m_AnimationTime = 0.8f;
    const float m_GoToMainTime = 0.2f;

    [SerializeField] ManagerSO m_MSO;
    [SerializeField] GameObject[] m_Menus;
    [SerializeField] GameObject[] m_FirstMenusButtons;
    [SerializeField] Animator m_MenuAnimator;
    [SerializeField] AudioSource[] m_Audios;
    [SerializeField] Button m_SoundButton;
    [SerializeField] Button m_PlayButton;
    [SerializeField] Button m_SkinsButton;
    [SerializeField] Button m_CreditsButton;
    [SerializeField] Button m_ExitButton;
    [SerializeField] EventSystem m_EventSystem;

    Rewired.Player m_PlayerInputManager;

    GameObject m_LastMenuButton;

    int m_CurrentMenu = 0;

    bool m_CanGo = true;

    void Awake()
    {
        m_PlayerInputManager = ReInput.players.GetPlayer("Player0");
        m_PlayerInputManager.controllers.maps.SetMapsEnabled(true, "Menu");
    }

    void Start()
    {
        AdvanceMenu(0);
    }

    private void Update()
    {
        m_SoundButton.image.sprite = m_MSO.generalInfo.soundButtonSprites[m_MSO.generalInfo.soundConfig.GetHashCode()];
        m_PlayButton.image.sprite = m_MSO.generalInfo.playButtonSprites[m_PlayButton.interactable.GetHashCode()];
        m_ExitButton.image.sprite = m_MSO.generalInfo.exitButtonSprites[m_ExitButton.interactable.GetHashCode()];
        m_CreditsButton.image.sprite = m_MSO.generalInfo.creditButtonSprites[m_CreditsButton.interactable.GetHashCode()];
        m_SkinsButton.image.sprite = m_MSO.generalInfo.skinsButtonSprites[m_SkinsButton.interactable.GetHashCode()];

        if (m_PlayerInputManager.GetButtonDown("Cancel") && m_CurrentMenu != 0 && m_CanGo)
        {
            if (m_CurrentMenu != 3)
            {
                CallMainAnimation();
            }
            PlayCancelAudio();
            BackMenu(0);
        }
    }

    public void ChangeButtonSprite()
    {
        m_LastMenuButton.GetComponent<Button>().interactable = !m_LastMenuButton.GetComponent<Button>().interactable;
    }

    public void AdvanceMenu(int menuNum)
    {
        m_LastMenuButton = m_EventSystem.currentSelectedGameObject;
        if (m_CanGo)
        {
            if (m_CurrentMenu == 0 && menuNum != 0)
            {
                ChangeButtonSprite();
            }
            else
            {
                m_Menus[m_CurrentMenu].SetActive(false);
            }
            m_CurrentMenu = menuNum;
            m_Menus[m_CurrentMenu].SetActive(true);
            if (m_FirstMenusButtons[menuNum] != null)
            {
                m_EventSystem.SetSelectedGameObject(m_FirstMenusButtons[menuNum]);
                m_FirstMenusButtons[menuNum].GetComponent<Button>().Select();
            }
        }
    }

    public void BackMenu(int menuNum)
    {
        if (m_CurrentMenu != 3)
        {
            WaitForAnimation();
            m_Menus[m_CurrentMenu].GetComponent<EnterInMenu>().CloseMenu();
        }

        ChangeButtonSprite();
        m_EventSystem.SetSelectedGameObject(m_LastMenuButton);
        m_LastMenuButton.GetComponent<Button>().Select();
        if (m_CurrentMenu == 3)
        {
            m_Menus[m_CurrentMenu].SetActive(false);
        }
        m_CurrentMenu = menuNum;
    }

    public void WaitForAnimation()
    {
        StartCoroutine(WaitForAnimationEnd());
    }

    public void CallMainAnimation()
    {
        StartCoroutine(GoMainAniamtion());
    }

    public void CallPopAnimation()
    {
        if (m_CanGo)
            m_MenuAnimator.SetTrigger("GoPop");
    }

    public void ChangeAudioState()
    {
        if (m_CanGo)
        {
            m_MSO.generalInfo.soundConfig = !m_MSO.generalInfo.soundConfig;
            PlayerPrefs.SetInt("SoundConfig", m_MSO.generalInfo.soundConfig.GetHashCode());
        }
    }

    public void QuitGameHandler()
    {
        if (m_CanGo)
            m_MSO.quitGame.Raise();
    }

    public void PlayGameHandler()
    {
        if (m_CanGo)
            m_MSO.playGame.Raise();
    }

    public void PlayConfirmAudio()
    {
        if (!m_Audios[0].isPlaying && m_CanGo)
            m_Audios[0].Play();
    }

    public void PlayCancelAudio()
    {
        if (!m_Audios[1].isPlaying && m_CanGo)
            m_Audios[1].Play();
    }

    IEnumerator GoMainAniamtion()
    {
        yield return new WaitForSeconds(m_GoToMainTime);
        m_MenuAnimator.SetTrigger("GoMain");
    }

    IEnumerator WaitForAnimationEnd()
    {
        m_CanGo = !m_CanGo;
        yield return new WaitForSeconds(m_AnimationTime);
        m_CanGo = !m_CanGo;
    }
}