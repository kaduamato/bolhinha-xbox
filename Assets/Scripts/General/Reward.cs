﻿using UnityEngine;
using UnityEngine.UI;

public class Reward : MonoBehaviour
{
    [SerializeField] ManagerSO m_MSO;
    [SerializeField] Image m_Sprite;
    [SerializeField] Text m_Message;
    [SerializeField] Text m_Score;
    [SerializeField] Text m_HighScore;
    [SerializeField] Animator m_PopUpAnimator;

    void Update()
    {
        m_Score.text = m_MSO.generalInfo.score.ToString();
        m_HighScore.text = $"High Score: {m_MSO.generalInfo.highScore.ToString()}";

        CheckNewHighScore();
        CheckReward();
       
    }

    void CheckNewHighScore()
    {
        if(m_MSO.generalInfo.highScore < m_MSO.generalInfo.score)
        {
            m_MSO.generalInfo.highScore = m_MSO.generalInfo.score;
            PlayerPrefs.SetInt("HighScore", m_MSO.generalInfo.highScore);
        }
    }

    void CheckReward()
    {
        if (m_MSO.generalInfo.unlockedSkins < m_MSO.skinKeeper.MySkins.Length)
        {
            if (m_MSO.generalInfo.highScore >= m_MSO.skinKeeper.MySkins[m_MSO.generalInfo.unlockedSkins].PriceAmount)
            {
                CallPopUp();
                m_MSO.generalInfo.unlockedSkins++;
                PlayerPrefs.SetInt("UnlockedSkins", m_MSO.generalInfo.unlockedSkins);
            }
        }
    }

    void CallPopUp()
    {
        m_Message.text = $"New Skin Unlocked: {m_MSO.skinKeeper.MySkins[m_MSO.generalInfo.unlockedSkins].SkinName}";
        m_Sprite.sprite = m_MSO.skinKeeper.MySkins[m_MSO.generalInfo.unlockedSkins].SkinSprite;
        m_PopUpAnimator.SetTrigger("SetPop");
    }
}
