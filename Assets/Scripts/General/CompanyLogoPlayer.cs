﻿using System.Collections;
using UnityEngine;

public class CompanyLogoPlayer : MonoBehaviour
{
    const float m_BreakAudioTime = 0.1f;
    const float m_MacucoAudioTime = 1.2f;
    const float m_ChangeSceneTime = 0.2f;

    [SerializeField] ManagerSO m_MSO;

    [SerializeField] AudioSource m_AudioSource;

    void Start()
    {
        StartCoroutine(PlayBreakEgg());
        StartCoroutine(PlayMacucoAudio());
    }

    IEnumerator PlayBreakEgg()
    {
        yield return new WaitForSeconds(m_BreakAudioTime);
        m_AudioSource.clip = m_MSO.generalInfo.breakEgg;
        m_AudioSource.Play();
    }

    IEnumerator PlayMacucoAudio()
    {
        yield return new WaitForSeconds(m_MacucoAudioTime);
        m_AudioSource.clip = m_MSO.generalInfo.macucoAudio;
        m_AudioSource.Play();
        StartCoroutine(OnSplashFinish());
    }

    IEnumerator OnSplashFinish()
    {
        yield return new WaitForSeconds(m_ChangeSceneTime);
        m_MSO.splashFinish.Raise();
    }
}
