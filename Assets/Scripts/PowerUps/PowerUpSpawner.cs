﻿using UnityEngine;

public class PowerUpSpawner : Spawner
{
    public override void SpawnObject(string powerUpTag, int direction)
    {
        GameObject powerUp = ObjectPooler.MyPooler.GetPooledObj(powerUpTag);
        PowerUp powerUpComponent = powerUp.GetComponent<PowerUp>();

        if (powerUp != null)
        {
            switch (direction)
            {
                case 0:
                    SetPowerUp(powerUp, powerUpComponent, 0, powerUpTag);
                    break;
                case 1:
                    SetPowerUp(powerUp, powerUpComponent, 1, powerUpTag);
                    break;
            }
        }
    }

    void SetPowerUp(GameObject powerUp, PowerUp powerUpComponent, int direction, string powerUpTag)
    {
        switch (direction)
        {
            case 0:
                powerUp.transform.position = new Vector3(m_MSO.obstaclesInfo.rightToLeftPos.x, Random.Range(m_MSO.obstaclesInfo.yLimitBottom, m_MSO.obstaclesInfo.yLimitTop), m_MSO.obstaclesInfo.rightToLeftPos.z);
                powerUp.transform.rotation = Quaternion.identity;
                powerUpComponent.Direction = 0;
                break;
            case 1:
                powerUp.transform.position = new Vector3(m_MSO.obstaclesInfo.leftToRightPos.x, Random.Range(m_MSO.obstaclesInfo.yLimitBottom, m_MSO.obstaclesInfo.yLimitTop), m_MSO.obstaclesInfo.leftToRightPos.z);
                powerUp.transform.rotation = Quaternion.identity;
                powerUpComponent.Direction = 1;
                break;
        }

        switch (powerUpTag)
        {
            case "Invincibility":
                powerUpComponent.Type = 1;
                break;
            case "TimeStop":
                powerUpComponent.Type = 2;
                break;
        }

        powerUp.SetActive(true);
    }
}