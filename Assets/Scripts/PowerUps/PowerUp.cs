﻿using UnityEngine;

public class PowerUp : SpawnableObject
{
    private int m_Type;
    public int Type { set => m_Type = value; }
    [SerializeField] AudioClip[] m_AudioClips;

    AudioSource m_Audio;

    const int m_PowerUpLayer = 10;
    const int m_EnemyLayer = 9;

    protected override void Start()
    {
        m_Audio = GameObject.Find("AudioPowerUp").GetComponent<AudioSource>();
    }

    protected override void Update()
    {
        Physics.IgnoreLayerCollision(m_PowerUpLayer, m_EnemyLayer, true);
        Physics.IgnoreLayerCollision(m_PowerUpLayer, m_PowerUpLayer, true);

        m_Speed = m_MSO.powerUpsInfo.powerUpSpeed * Time.deltaTime;

        switch (m_Direction)
        {
            case 0:
                transform.position += Vector3.left * m_Speed;
                break;
            case 1:
                transform.position += Vector3.right * m_Speed;
                break;
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            switch (m_Type)
            {
                case 1:
                    m_Audio.clip = m_AudioClips[0];
                    break;
                case 2:
                    m_Audio.clip = m_AudioClips[1];
                    break;
            }
            if (!m_Audio.isPlaying)
                m_Audio.Play();
            m_MSO.powerUpsInfo.type = m_Type;
            m_MSO.powerUpCollision.Raise();
            gameObject.SetActive(false);
        }    
    }
}