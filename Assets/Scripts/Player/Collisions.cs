﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Collisions : MonoBehaviour
{
    [SerializeField] ManagerSO m_MSO;
    [SerializeField] Animator m_Animator;
    [SerializeField] AudioSource[] m_Audios;
    [SerializeField] GameObject m_Shield;
    [SerializeField] Image m_PowerUpEffect;

    const float m_DeathAnimationTime = 2.0f;
    const float m_MasterMuteVolume = -80.0f;
    const float m_MasterUnmuteVolume = 0.0f;
    const float m_NormalTimeScale = 1.0f;
    const float m_TimeStopTimeScale = 0.25f;

    SphereCollider m_SphereCollider;
    Animator m_EffectAnimator;

    void Start()
    {
        m_SphereCollider = GetComponent<SphereCollider>();
        m_EffectAnimator = m_PowerUpEffect.GetComponent<Animator>();
    }

    void Update()
    {
        m_SphereCollider.enabled = !m_MSO.generalInfo.isGameOver;

        if (m_MSO.generalInfo.isGameOver)
        {
            m_Shield.gameObject.SetActive(false);
            m_PowerUpEffect.gameObject.SetActive(false);
            Time.timeScale = m_NormalTimeScale;
        }
    }

    public void OnPowerUpCollision()
    {
        m_MSO.player.playerState = m_MSO.powerUpsInfo.type;
        m_MSO.generalInfo.master.SetFloat("GameplayTracks", m_MasterMuteVolume);
        StartCoroutine(PowerUpDuration());
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            if (m_MSO.player.playerState != 1)
            {
                if (!m_Audios[0].isPlaying)
                    m_Audios[0].Play();
                m_MSO.generalInfo.master.SetFloat("GameplayTracks", m_MasterMuteVolume);
                m_Animator.SetTrigger("IsDead");
                m_MSO.generalInfo.isGameOver = true;
                StartCoroutine(DeathAnimationFinish());
            }
        }
    }

    IEnumerator PowerUpDuration()
    {
        switch (m_MSO.player.playerState)
        {
            case 1:
                m_PowerUpEffect.color = Color.yellow;
                m_Shield.SetActive(true);
                break;
            case 2: 
                m_PowerUpEffect.color = Color.cyan;
                Time.timeScale = m_TimeStopTimeScale;
                break;
        }
        m_PowerUpEffect.gameObject.SetActive(true);
        m_EffectAnimator.SetTrigger("Pulse");

        yield return new WaitForSecondsRealtime(m_MSO.powerUpsInfo.powerUpDuration);

        m_MSO.player.playerState = 0;
        m_PowerUpEffect.gameObject.SetActive(false);
        m_MSO.generalInfo.master.SetFloat("GameplayTracks", m_MasterUnmuteVolume);
        m_Shield.SetActive(false);
        Time.timeScale = m_NormalTimeScale;
    }

    IEnumerator DeathAnimationFinish()
    {
        yield return new WaitForSeconds(m_DeathAnimationTime);
        m_MSO.death.Raise();
    }
}