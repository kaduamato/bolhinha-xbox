﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class SwitchSide : MonoBehaviour
{
    [SerializeField] ManagerSO m_MSO;
    [SerializeField] Animator m_Animator, m_AnimatorRot;
    [SerializeField] float m_Speed;
    [SerializeField] AudioSource[] m_Audios;
    [SerializeField] AudioClip[] m_TerminalsClips;
    [SerializeField] Text m_Score;
    [SerializeField] GameObject m_Terminal1;
    [SerializeField] GameObject m_Terminal2;

    const float m_AudioSourcesMaxVolume = 0.4f;
    const float m_DecreaseScoreCounter = 10.0f;
    const int m_DecreaseScoreRate = 1;

    private bool m_IsSwitching;
    public bool IsSwitching { set => m_IsSwitching = value; }

    bool m_CanIncScore = true;
    bool m_CanPlayAnimation;
    bool m_CanDecreaseScore;
    bool m_CanCallDecreaseCoroutine;

    float m_TempSpeed;

    Coroutine m_DecreaseScoreCoroutine;

    Rewired.Player m_Player;

    void Awake()
    {
        m_Player = ReInput.players.GetPlayer("Player0");
    }

    void Start()
    {
        m_TempSpeed = m_Speed;
        m_MSO.player.playerSpeed = m_TempSpeed;
        transform.position = m_MSO.player.terminal1Pos;
        m_Audios[0].volume = m_AudioSourcesMaxVolume;
        for (int i = 1; i < m_Audios.Length; ++i)
        {
            m_Audios[i].volume = 0.0f;
        }
        m_DecreaseScoreCoroutine = StartCoroutine(DecreaseScore());

        ActivateTerminalTriggers(false, true);
    }

    void Update()
    {
        m_Score.text = m_MSO.generalInfo.score.ToString();

        if (m_MSO.generalInfo.score < 0)
        {
            m_MSO.generalInfo.score = 0;
        }

        if (m_CanDecreaseScore)
        {
            if (m_CanCallDecreaseCoroutine)
            {
                StartCoroutine(Decrease());
                m_CanCallDecreaseCoroutine = false;
            }
        }

        if (m_MSO.generalInfo.isGameOver)
        {
            m_IsSwitching = false;
            m_MSO.player.playerSpeed = 0.0f;
            StopCoroutine(m_DecreaseScoreCoroutine);
        }
        else
        {
            m_MSO.player.playerSpeed = m_TempSpeed;
        }

        if (Vector3.Distance(transform.position, m_MSO.player.terminal1Pos) < m_MSO.player.minDistToTerminal)
        {
            m_CanIncScore = true;
            SetTerminalState(0);
        }
        if (Vector3.Distance(transform.position, m_MSO.player.terminal2Pos) < m_MSO.player.minDistToTerminal)
        {
            m_CanIncScore = true;
            SetTerminalState(1);
        }

        if (!m_IsSwitching && !m_MSO.generalInfo.isGameOver && !m_MSO.generalInfo.isPaused && m_Player.GetButtonDown("Switch Side"))
        {
            m_IsSwitching = true;
            m_CanPlayAnimation = true;
            switch (m_MSO.player.terminalState)
            {
                case 0:
                    m_Audios[0].clip = m_TerminalsClips[0];
                    break;
                case 1:
                    m_Audios[0].clip = m_TerminalsClips[1];
                    break;
            }
            if (!m_Audios[0].isPlaying)
            {
                m_Audios[0].Play();
            }
            m_Animator.SetTrigger("IsLeaving");
            m_AnimatorRot.SetTrigger("ChangeRot");
        }

        if (m_MSO.generalInfo.score == m_MSO.generalInfo.tracksStartScores[0])
        {
            m_Audios[1].volume = m_AudioSourcesMaxVolume;
        }
        if (m_MSO.generalInfo.score == m_MSO.generalInfo.tracksStartScores[1])
        {
            m_Audios[2].volume = m_AudioSourcesMaxVolume;
        }

        if (m_IsSwitching)
        {
            if (m_MSO.player.terminalState == 0)
            {
                SwitchTerminal(1);
            }
            if (m_MSO.player.terminalState == 1)
            {
                SwitchTerminal(0);
            }
        }
    }
    
    void SwitchTerminal(int terminal)
    {
        switch (terminal)
        {
            case 0:
                transform.position = Vector3.Lerp(transform.position, m_MSO.player.terminal1Pos, m_MSO.player.playerSpeed * Time.unscaledDeltaTime);
                break;
            case 1:
                transform.position = Vector3.Lerp(transform.position, m_MSO.player.terminal2Pos, m_MSO.player.playerSpeed * Time.unscaledDeltaTime);
                break;
        }

        if (m_CanIncScore)
        {
            m_CanIncScore = false;
        }

        m_CanDecreaseScore = false;
        m_CanCallDecreaseCoroutine = false;
        StopCoroutine(m_DecreaseScoreCoroutine);
        m_DecreaseScoreCoroutine = StartCoroutine(DecreaseScore());
    }

    void SetTerminalState(int terminalState)
    {
        m_MSO.player.terminalState = terminalState;
        m_IsSwitching = false;
        if (m_CanPlayAnimation)
        {
            m_Animator.SetTrigger("IsArriving");
            m_CanPlayAnimation = false;
        }
    }

    void ActivateTerminalTriggers(bool terminal1Active, bool terminal2Active)
    {
        m_Terminal1.SetActive(terminal1Active);
        m_Terminal2.SetActive(terminal2Active);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Terminal"))
        {
            ++m_MSO.generalInfo.score;
            switch (other.gameObject.name)
            {
                case "Terminal1":
                    ActivateTerminalTriggers(false, true);
                    break;
                case "Terminal2":
                    ActivateTerminalTriggers(true, false);
                    break;
            }
        }    
    }

    IEnumerator DecreaseScore()
    {
        yield return new WaitForSeconds(m_DecreaseScoreCounter);
        m_CanDecreaseScore = true;
        m_CanCallDecreaseCoroutine = true;
    }

    IEnumerator Decrease()
    {
        yield return new WaitForSeconds(m_DecreaseScoreRate);
        --m_MSO.generalInfo.score;

        if (m_CanDecreaseScore)
        {
            StartCoroutine(Decrease());
        }
    }
}