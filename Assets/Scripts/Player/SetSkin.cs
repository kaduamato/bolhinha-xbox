﻿using UnityEngine;

public class SetSkin : MonoBehaviour
{
    [SerializeField] ManagerSO m_MSO;
    [SerializeField] SkinnedMeshRenderer m_Skin;
    [SerializeField] Light m_MyLight;
    [SerializeField] TrailRenderer m_MyTrail;

    void Start()
    {
        m_Skin.material.mainTexture = m_MSO.skinKeeper.MySkins[m_MSO.generalInfo.playerSkin].SkinTexture;
        m_MyLight.color = m_MSO.skinKeeper.MySkins[m_MSO.generalInfo.playerSkin].LightColor;
        m_MyTrail.colorGradient = m_MSO.skinKeeper.MySkins[m_MSO.generalInfo.playerSkin].SkinTrailGradient;
    }
}