﻿using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "ScriptableObjects/PowerUp", order = 4)]
public class PowerUpsSO : ScriptableObject
{
    public float powerUpSpeed;
    public float powerUpDuration;
    public int type;
}