﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "ScriptableObjects/GameEvent", order = 1)]
public class GameEventSO : ScriptableObject
{
	List<GameEventListener> listeners = new List<GameEventListener>();
	public string eventName;

	public void Raise()
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
			listeners[i].OnEventRaised("On"+eventName);
	}

	public void RegisterListener(GameEventListener listener)
	{ 
		listeners.Add(listener); 
	}

	public void UnregisterListener(GameEventListener listener)
	{ 
		listeners.Remove(listener); 
	}
}
