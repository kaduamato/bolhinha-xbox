﻿using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "ScriptableObjects/SpawnManager", order = 6)]
public class SpawnManagerSO : ScriptableObject
{
    public int[] scoreLevels;
    public int currentLevel;
    public float powerUpSpawnRate;
    public float[] levelsObsSpawnRates;
    public string[] obstacles;
    public string[] powerUps;
}