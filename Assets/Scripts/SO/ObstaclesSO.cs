﻿using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "ScriptableObjects/Obstacles", order = 3)]
public class ObstaclesSO : ScriptableObject
{
    public Vector3 rightToLeftPos;
    public Vector3 leftToRightPos;
    public Vector3 dLeftUpToRightDownPos;
    public Vector3 dLeftDownToRightUpPos;
    public Vector3 dRightUpToLeftDownPos;
    public Vector3 dRightDownToLeftUpPos;
    public Vector3 leftUpToRightDownShortcut;
    public Vector3 leftDownToRightUpShortcut;
    public Vector3 rightUpToLeftDownShortcut;
    public Vector3 rightDownToLeftUpShortcut;
    public float obstaclesSpeed;
    public float yLimitTop;
    public float yLimitBottom;
    public float[] rightOrientation;
    public float[] leftOrientation;
    public float[] leftUpToRightDownOrientation;
    public float[] leftDownToRightUpOrientation;
    public float[] rightUpToLeftDownOrientation;
    public float[] rightDownToLeftUpOrientation;
    public Texture2D[] catTextures;
}