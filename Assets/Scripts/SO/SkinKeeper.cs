﻿using UnityEngine;

[CreateAssetMenu(fileName = "AllSkins", menuName = "ScriptableObjects/SkinInfos")]
public class SkinKeeper : ScriptableObject
{
    public SkinData[] MySkins;
}

[System.Serializable]
public class SkinData
{
    public Texture2D SkinTexture;
    public Gradient SkinTrailGradient;
    public Color LightColor;
    public int PriceAmount;
    public Sprite SkinSprite;
    public string SkinName;
}