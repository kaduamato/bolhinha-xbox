﻿using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "ScriptableObjects/Player", order = 2)]
public class PlayerSO : ScriptableObject
{
    public int playerState;
    public int terminalState;
    public float playerSpeed;
    public float minDistToTerminal;
    public Vector3 terminal1Pos;
    public Vector3 terminal2Pos;
    public Quaternion rot1;
    public Quaternion rot2;
}