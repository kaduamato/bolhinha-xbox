﻿using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "ScriptableObjects/Manager", order = 0)]
public class ManagerSO : ScriptableObject
{
    [Header("GameEvents")]
    public GameEventSO gamePaused;
    public GameEventSO gameResume;
    public GameEventSO quitGame;
    public GameEventSO playGame;
    public GameEventSO powerUpCollision;
    public GameEventSO death;
    public GameEventSO restartGame;
    public GameEventSO backMenu;
    public GameEventSO splashFinish;

    [Header("Entities")]
    public PlayerSO player;

    [Header("General")]
    public GeneralInfoSO generalInfo;
    public SkinKeeper skinKeeper;

    [Header("Spawners")]
    public ObstaclesSO obstaclesInfo;
    public PowerUpsSO powerUpsInfo;
    public SpawnManagerSO spawnManagerInfo;
}