﻿using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "ScriptableObjects/General Info", order = 5)]
public class GeneralInfoSO : ScriptableObject
{
    [Header("General")]
    public float loadTime;
    public bool isGameOver;
    public float controllerDelay;
    public bool isPaused;

    [Header("Audio")]
    public AudioMixer master;
    public AudioClip breakEgg;
    public AudioClip macucoAudio;

    [Header("Points")]
    public int score;
    public int highScore;
    public int unlockedSkins;
    public int playerSkin;
    public int[] tracksStartScores;

    [Header("Configs")]
    public bool soundConfig;

    [Header("UI")]
    public Sprite[] statusSprites;
    public Sprite blockedSkin;
    public string blockedSkinName;
    public Sprite[] soundButtonSprites;
    public Sprite[] playButtonSprites;
    public Sprite[] skinsButtonSprites;
    public Sprite[] creditButtonSprites;
    public Sprite[] exitButtonSprites;
}